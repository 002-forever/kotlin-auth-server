package boris.project.loki.auth.server.configs

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
class CorsFilter : Filter {
  @Throws(IOException::class, ServletException::class)
  override fun doFilter(
    req: ServletRequest,
    res: ServletResponse,
    chain: FilterChain
  ) {
    val response = res as HttpServletResponse
    response.setHeader("Access-Control-Allow-Origin", "http://localhost:8080")

    // without this header jquery.ajax calls returns 401 even after successful login and SSESSIONID being succesfully stored.
    response.setHeader("Access-Control-Allow-Credentials", "true")
    response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE")
    response.setHeader("Access-Control-Max-Age", "3600")
    response.setHeader(
        "Access-Control-Allow-Headers",
        "X-Requested-With, Authorization, Origin, Content-Type, Version")
    response.setHeader(
        "Access-Control-Expose-Headers",
        "X-Requested-With, Authorization, Origin, Content-Type")
    val request = req as HttpServletRequest
    if (request.method != "OPTIONS") {
      chain.doFilter(req, res)
    } else {
      response.setHeader("Access-Control-Allow-Origin", "http://localhost:8080")
      response.setHeader("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT")
      response.setHeader("Access-Control-Max-Age", "3600")
      response.setHeader(
          "Access-Control-Allow-Headers",
          "Access-Control-Expose-Headers" + "Authorization, content-type," +
              "USERID" + "ROLE" +
              "access-control-request-headers,access-control-request-method,accept,origin,authorization,x-requested-with,responseType,observe")
      response.status = HttpServletResponse.SC_OK
    }
  }

  override fun destroy() {}

  @Throws(ServletException::class)
  override fun init(filterConfig: FilterConfig) {
  }
}