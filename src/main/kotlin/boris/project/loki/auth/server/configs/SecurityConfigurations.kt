package boris.project.loki.auth.server.configs

import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.access.channel.ChannelProcessingFilter

@Configuration
@EnableWebSecurity
class SecurityConfigurations(
  val authenticationProvider: AuthenticationProvider,
  val corsFilter: CorsFilter
) :
  WebSecurityConfigurerAdapter() {

  override fun configure(auth: AuthenticationManagerBuilder) {
    auth.authenticationProvider(authenticationProvider)
  }

  override fun configure(http: HttpSecurity) {
    http.addFilterBefore(corsFilter, ChannelProcessingFilter::class.java)
        .httpBasic().disable()
        .csrf().disable()
        .authorizeRequests()
        .anyRequest().authenticated()
  }
}