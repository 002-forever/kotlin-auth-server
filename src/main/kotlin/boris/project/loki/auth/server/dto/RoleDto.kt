package boris.project.loki.auth.server.dto

class RoleDto(val id: Long, val name: String)