package boris.project.loki.auth.server.extensions

fun Any?.isNull(): Boolean {
  return this == null
}