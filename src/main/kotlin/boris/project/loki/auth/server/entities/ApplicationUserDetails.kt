package boris.project.loki.auth.server.entities

import boris.project.loki.auth.server.dto.RoleDto
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

class ApplicationUserDetails(
  val id: Long,
  val email: String,
  private val username: String,
  private val password: String,
  val name: String,
  val surname: String,
  val status: Status,
  val roles: Set<RoleDto>,
  val created: Date,
  val updated: Date
) : UserDetails {

  override fun getAuthorities(): Collection<GrantedAuthority> {
    return roles.map { SimpleGrantedAuthority(it.name) }.toList()
  }

  override fun isEnabled(): Boolean {
    return status == Status.ACTIVE
  }

  override fun getUsername(): String {
    return username
  }

  override fun isCredentialsNonExpired(): Boolean {
    return true
  }

  override fun getPassword(): String {
    return password
  }

  override fun isAccountNonExpired(): Boolean {
    return true
  }

  override fun isAccountNonLocked(): Boolean {
    return true
  }
}