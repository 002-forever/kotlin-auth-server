package boris.project.loki.auth.server.entities

import boris.project.loki.auth.server.dto.UpdateUser
import boris.project.loki.auth.server.dto.UserDto
import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*

@Entity
@Table(name = "users")
class User(
  var email: String,
  var username: String,
  var password: String,
  var name: String,
  var surname: String,

  @JsonManagedReference
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_roles",
      joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
      inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")]
  )
  var roles: MutableSet<Role> = hashSetOf()
) : BaseEntity<Long>() {
  fun update(updateUser: UpdateUser) {
    updateUser.email?.let { email = it }
    updateUser.username?.let { username = it }
    updateUser.password?.let { password = it }
    updateUser.name?.let { name = it }
    updateUser.surname?.let { surname = it }
    updateUser.status?.let { status = it }
  }

  fun toUserDto(): UserDto {
    return UserDto(
        id!!,
        email,
        username,
        name,
        surname,
        status!!,
        roles.map { it.toRoleDto() }.toSet(),
        dateCreated!!,
        dateUpdated!!)
  }

  fun toUserDetails(): ApplicationUserDetails {
    return ApplicationUserDetails(
        id!!,
        email,
        username,
        password,
        name,
        surname,
        status!!,
        roles.map { it.toRoleDto() }.toSet(),
        dateCreated!!,
        dateUpdated!!)
  }
}