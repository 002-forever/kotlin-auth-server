package boris.project.loki.auth.server.configs

import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.KeyUse
import com.nimbusds.jose.jwk.RSAKey
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.security.KeyPair
import java.security.interfaces.RSAPublicKey

@Configuration
class JwkConfigs {

    @Bean
    fun jwkSet(keyPair: KeyPair): JWKSet {
        var builder = RSAKey.Builder(keyPair.public as RSAPublicKey)
                .keyUse(KeyUse.SIGNATURE)
                .algorithm(JWSAlgorithm.RS256)
                .keyID("boris-key-id")
        return JWKSet(builder.build())
    }
}