package boris.project.loki.auth.server.dto

import boris.project.loki.auth.server.entities.Status
import java.util.*

class UserDto(
  val id: Long,
  val email: String,
  val username: String,
  val name: String,
  val surname: String,
  val status: Status,
  val roles: Set<RoleDto>,
  val created: Date,
  val updated: Date
)