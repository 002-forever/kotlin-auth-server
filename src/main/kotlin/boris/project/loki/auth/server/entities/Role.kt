package boris.project.loki.auth.server.entities

import boris.project.loki.auth.server.dto.RoleDto
import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.ManyToMany
import javax.persistence.Table

@Entity
@Table(name = "roles")
class Role(
  val name: String,

  @JsonBackReference
  @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
  val users: Set<User> = hashSetOf()
) : BaseEntity<Long>() {
  fun toRoleDto(): RoleDto {
    return RoleDto(id!!, name)
  }
}