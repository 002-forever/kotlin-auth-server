package boris.project.loki.auth.server.controllers

import boris.project.utils.info.response.Info
import boris.project.utils.info.response.InfoFactory
import mu.KLogging
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/public")
class StatusController(val info: InfoFactory) {
    companion object: KLogging()
    @GetMapping("/status")
    fun status(): ResponseEntity<Info> = ResponseEntity.ok(info.createServiceInfo("auth-service"))
}
