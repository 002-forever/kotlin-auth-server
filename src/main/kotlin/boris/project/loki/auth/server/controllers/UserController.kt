package boris.project.loki.auth.server.controllers

import boris.project.loki.auth.server.dto.RegisterUser
import boris.project.loki.auth.server.dto.UpdateUser
import boris.project.loki.auth.server.services.interfaces.IUserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/users")
class UserController(val userService: IUserService) {

  @GetMapping("/all")
  fun getAll() = userService.getAll().map { it.toUserDto() }

  @PostMapping("/register")
  fun register(@RequestBody registerUser: RegisterUser) =
      userService.create(registerUser).toUserDto()

  @PutMapping
  fun update(@RequestParam id: Long, @RequestBody updateUser: UpdateUser) =
      userService.update(id, updateUser).toUserDto()

  @DeleteMapping
  fun delete(id: Long) = userService.delete(id).toUserDto()

}