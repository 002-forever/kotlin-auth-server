package boris.project.loki.auth.server.configs

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class WebMvcConfigurerAdapter : WebMvcConfigurer {
  override fun addViewControllers(registry: ViewControllerRegistry) {
    registry.addViewController("/login").setViewName("forward:/login.html")
    super.addViewControllers(registry)
  }

  override fun addCorsMappings(registry: CorsRegistry) {
    registry.addMapping("/**")
        .allowedOrigins("*")
        .allowedMethods("*")
  }
}