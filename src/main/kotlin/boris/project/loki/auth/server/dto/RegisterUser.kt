package boris.project.loki.auth.server.dto

import boris.project.loki.auth.server.entities.User

class RegisterUser(
  val email: String,
  val username: String,
  val password: String,
  val name: String,
  val surname: String
) {
  fun toUser(): User {
    return User(email, username, password, name, surname)
  }
}