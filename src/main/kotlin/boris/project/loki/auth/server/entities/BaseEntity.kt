package boris.project.loki.auth.server.entities

import boris.project.loki.auth.server.extensions.isNull
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.domain.Persistable
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.util.*
import javax.persistence.*

@EntityListeners(AuditingEntityListener::class)
@MappedSuperclass
abstract class BaseEntity<T : Serializable> : Persistable<T> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private val id: T? = null

  @CreatedDate
  @Column(name = "created")
  var dateCreated: Date? = null

  @LastModifiedDate
  @Column(name = "updated")
  var dateUpdated: Date? = null

  @Enumerated(EnumType.STRING)
  @Column(name = "status")
  var status: Status? = null

  override fun isNew(): Boolean {
    return id.isNull()
  }

  override fun toString(): String {
    return "BaseEntity(id=$id, dateCreated=$dateCreated, dateUpdated=$dateUpdated, status=$status)"
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false
    other as BaseEntity<*>
    if (id != other.id) return false
    return true
  }

  override fun hashCode(): Int {
    return 111
  }

  override fun getId(): T? {
    return id
  }
}