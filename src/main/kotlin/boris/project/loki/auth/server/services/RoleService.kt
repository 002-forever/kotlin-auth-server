package boris.project.loki.auth.server.services

import boris.project.loki.auth.server.entities.Role
import boris.project.loki.auth.server.repositories.RoleRepository
import boris.project.loki.auth.server.services.interfaces.IRoleService
import org.springframework.stereotype.Service

@Service
class RoleService(val roleRepository: RoleRepository) : IRoleService {
  override fun getByName(name: String): Role {
    return roleRepository.findByName(name)
  }

  override fun getAll(): Collection<Role> {
    return roleRepository.findAll()
  }
}