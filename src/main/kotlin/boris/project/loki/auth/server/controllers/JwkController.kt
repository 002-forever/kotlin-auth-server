package boris.project.loki.auth.server.controllers

import com.nimbusds.jose.jwk.JWKSet
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class JwkController(val jwkSet: JWKSet) {

    @GetMapping("/.well-known/jwks.json")
    fun keys() = jwkSet.toJSONObject()
}