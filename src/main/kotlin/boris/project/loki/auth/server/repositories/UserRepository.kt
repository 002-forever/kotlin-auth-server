package boris.project.loki.auth.server.repositories

import boris.project.loki.auth.server.entities.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Long> {
  fun findByUsername(username: String): User?
}