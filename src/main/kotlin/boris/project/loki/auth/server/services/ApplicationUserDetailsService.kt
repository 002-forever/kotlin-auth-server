package boris.project.loki.auth.server.services

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class ApplicationUserDetailsService(val userService: UserService) : UserDetailsService {
  override fun loadUserByUsername(username: String): UserDetails {
    return userService.getByUsername(username).toUserDetails()
  }
}