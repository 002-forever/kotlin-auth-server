package boris.project.loki.auth.server.controllers

import boris.project.loki.auth.server.services.RoleService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/roles")
class RolesController(val roleService: RoleService) {

  @GetMapping
  fun getAll() = roleService.getAll().map { it.toRoleDto() }
}