package boris.project.loki.auth.server.dto

import boris.project.loki.auth.server.entities.Status

class UpdateUser(
  val email: String? = null,
  val username: String? = null,
  val password: String? = null,
  val name: String? = null,
  val surname: String? = null,
  val status: Status? = null,
  val roles: Set<String>? = null
)