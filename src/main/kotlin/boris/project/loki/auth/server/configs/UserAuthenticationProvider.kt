package boris.project.loki.auth.server.configs

import boris.project.loki.auth.server.services.UserService
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class UserAuthenticationProvider(
  val userService: UserService,
  val passwordEncoder: PasswordEncoder
) : AuthenticationProvider {
  override fun authenticate(authentication: Authentication): Authentication {
    val username = authentication.name
    val password = authentication.credentials as String
    val user = userService.getByUsername(username).toUserDetails()
    if (user.username == username || user.name == username) {
      if (!passwordEncoder.matches(password, user.password)) {
        throw BadCredentialsException("Wrong password.")
      }
      val authorities = user.authorities
      return UsernamePasswordAuthenticationToken(user, password, authorities)
    } else {
      throw BadCredentialsException("User not found.")
    }
  }

  override fun supports(authentication: Class<*>): Boolean {
    return authentication.equals(UsernamePasswordAuthenticationToken::class.java)
  }
}