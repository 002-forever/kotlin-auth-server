package boris.project.loki.auth.server.controllers

import boris.project.loki.auth.server.services.interfaces.IUserService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/api/user")
class MeController(val userService: IUserService) {
  @GetMapping("/me")
  fun me(principal: Principal) = userService.getByUsername(principal.name).toUserDto()
}