package boris.project.loki.auth.server.configs

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory
import java.security.KeyPair
import java.util.*


@Configuration
class JwtConfigs {

    companion object {
        private const val JWT_KEY_FILE = "boris-jwt.jks"
        private const val JWT_KEY_PASS = "boris-pass"
        private const val JWT_KEY_ALIAS = "boris-oauth-jwt"
    }

    @Bean
    fun jwtKeyPair(): KeyPair {
        val ksFile = ClassPathResource(JWT_KEY_FILE)
        val ksFactory = KeyStoreKeyFactory(ksFile,JWT_KEY_PASS.toCharArray())
        return ksFactory.getKeyPair(JWT_KEY_ALIAS)
    }

    @Bean
    fun accessTokenConverter(keyPair: KeyPair): JwtAccessTokenConverter {
        val customHeaders = Collections.singletonMap("kid", "boris-key-id")
        return JwtCustomHeadersAccessTokenConverter(
                customHeaders,
                keyPair)
    }

    @Bean
    fun tokenStore(accessTokenConverter: JwtAccessTokenConverter) = JwtTokenStore(accessTokenConverter)
}