package boris.project.loki.auth.server.repositories

import boris.project.loki.auth.server.entities.Role
import org.springframework.data.jpa.repository.JpaRepository

interface RoleRepository : JpaRepository<Role, Long> {
  fun findByName(name: String): Role
}