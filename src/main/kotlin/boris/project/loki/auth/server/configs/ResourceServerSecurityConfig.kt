package boris.project.loki.auth.server.configs

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter

@Configuration
class ResourceServerSecurityConfig : ResourceServerConfigurerAdapter() {
  private val whiteList = arrayOf(
      "/public/**",
      "/.well-known/**",
      "/api/users/register",
      "/v2/api-docs",
      "/configuration/ui",
      "/swagger-resources/**",
      "/configuration/security",
      "/swagger-ui.html",
      "/webjars/**"
  )

  override fun configure(http: HttpSecurity) {
    http.authorizeRequests()
        .antMatchers(*whiteList).permitAll()
        .antMatchers("/api/user/me").hasAnyRole("USER", "ADMIN")
        .antMatchers("/api/users/all").hasRole("ADMIN")
  }
}