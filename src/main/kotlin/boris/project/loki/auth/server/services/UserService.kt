package boris.project.loki.auth.server.services

import boris.project.loki.auth.server.dto.RegisterUser
import boris.project.loki.auth.server.dto.UpdateUser
import boris.project.loki.auth.server.entities.Status
import boris.project.loki.auth.server.entities.User
import boris.project.loki.auth.server.exceptions.UserNotFoundException
import boris.project.loki.auth.server.repositories.RoleRepository
import boris.project.loki.auth.server.repositories.UserRepository
import boris.project.loki.auth.server.services.interfaces.IUserService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService(
  val userRepository: UserRepository,
  val roleRepository: RoleRepository,
  val passwordEncoder: PasswordEncoder
) :
  IUserService {
  override fun create(registerUser: RegisterUser): User {
    val user = registerUser.toUser().apply {
      status = Status.ACTIVE
      roles = mutableSetOf(roleRepository.findByName("ROLE_USER"))
      password = passwordEncoder.encode(registerUser.password)
    }
    return userRepository.save(user)
  }

  override fun update(id: Long, updateUser: UpdateUser): User {
    val user = getById(id)
    user.update(updateUser)
    updateUser.roles?.let {
      user.roles = it.map { roleName -> roleRepository.findByName(roleName) }.toMutableSet()
    }
    return userRepository.save(user)
  }

  override fun delete(id: Long): User {
    return update(id, UpdateUser(status = Status.DELETED))
  }

  override fun getAll(): Collection<User> {
    return userRepository.findAll()
  }

  override fun getById(id: Long): User {
    return userRepository.findById(id).orElseThrow {
      throw UserNotFoundException("User with id $id not found.")
    }
  }

  override fun getByUsername(username: String): User {
    return userRepository.findByUsername(username)
      ?: throw UserNotFoundException("User with username $username not found.")
  }

}