package boris.project.loki.auth.server.entities

enum class Status {
  ACTIVE, NOT_ACTIVE, DELETED
}