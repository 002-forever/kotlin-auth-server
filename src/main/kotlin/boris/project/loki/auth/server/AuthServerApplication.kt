package boris.project.loki.auth.server

import boris.project.utils.info.response.InfoFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer

@SpringBootApplication
@EnableAuthorizationServer
@EnableJpaAuditing
@EnableResourceServer
@Import(InfoFactory::class)
class AuthServerApplication

fun main(args: Array<String>) {
  runApplication<AuthServerApplication>(*args)
}
