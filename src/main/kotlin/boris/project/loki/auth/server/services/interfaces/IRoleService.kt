package boris.project.loki.auth.server.services.interfaces

import boris.project.loki.auth.server.entities.Role

interface IRoleService {
  fun getByName(name: String): Role
  fun getAll(): Collection<Role>
}