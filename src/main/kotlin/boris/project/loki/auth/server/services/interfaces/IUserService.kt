package boris.project.loki.auth.server.services.interfaces

import boris.project.loki.auth.server.dto.RegisterUser
import boris.project.loki.auth.server.dto.UpdateUser
import boris.project.loki.auth.server.entities.User

interface IUserService {
  fun getAll(): Collection<User>
  fun getById(id: Long): User
  fun getByUsername(username: String): User
  fun create(registerUser: RegisterUser): User
  fun update(id: Long, updateUser: UpdateUser): User
  fun delete(id: Long): User
}